# Additional scripts for De-Bloater in Warden

[*Warden*](https://gitlab.com/AuroraOSS/AppWarden) is a FOSS app management utility. This repo has additional debloat scripts in .json format which work for Warden. Thanks to the very extensive and detailed android package lists maintained by W1nst0n.


## De-Bloater (Requires Root)
Warden provides a profile based de-bloater where a profile is created in a format specified as in this sample scripts
You need to place this profile/your custom profile at `ExternalStorage/Warden/Profiles` to make them appear in app.

De-Bloater is an experimental feature, will improve it over time.
Default action for debloating is 'disable' you can configure it to 'uninstall' or 'hide' from settings.

## Notes

The following packages were left out of google_extreme_debloat.json because I use these personally or would break my personal setup. To also debloat these, add them to the script.
```
"com.google.android.apps.chromecast.app",
"com.google.android.configupdater",
"com.google.android.setupwizard",
"com.google.android.setupwizard.a_overlay",
"com.google.android.pixel.setupwizard",
"com.google.android.onetimeinitializer",
"com.google.android.syncadapters.calendar",
"com.google.android.syncadapters.contacts",
"com.google.android.youtube"
```

# Credits

* [Aurora OSS - Warden](https://gitlab.com/AuroraOSS/AppWarden)
* [Universal Android Debloater](https://gitlab.com/W1nst0n/universal-android-debloater) 
